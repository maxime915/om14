"load secrets for the application from the environment"

import os

def SECRET_KEY() -> str:
    "get the django secret key"
    return os.environ['DJANGO_OM14_SECRET']

def DATABASE_PASSWORD() -> str:
    "get the password for the main user of the database"
    return os.environ['DJANGO_OM14_DB_MAIN']
